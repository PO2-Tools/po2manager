/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.model;


import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.utils.DataNodeType;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inrae.po2engine.model.dataModel.GeneralFile;
import fr.inrae.po2engine.model.dataModel.GenericFile;
import fr.inrae.po2engine.model.dataModel.ItineraryFile;
import fr.inrae.po2engine.model.dataModel.StepFile;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ContextMenu;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * @author stephane
 */
public class DataNode implements Comparable<DataNode> {

    private ObservableList<DataNode> listSubNode;
    private ItineraryFile itineraryPart;
    private StringProperty name;
    private ArrayList<DataNode> fathers;
    private GenericFile file;
    private DataNodeType type;
    private ImageView imageView;
    Image process = UITools.getImage("resources/images/treeview/process.png");
    Image process_loaded = UITools.getImage("resources/images/treeview/process_loaded.png");
    Image step = UITools.getImage("resources/images/treeview/step.png");
    Image obs = UITools.getImage("resources/images/treeview/observation.png");
    Image iti = UITools.getImage("resources/images/treeview/itinerary.png");



    public DataNode(DataNodeType type) {
        this.imageView = new ImageView();
        this.listSubNode = FXCollections.observableArrayList();
        this.name = new SimpleStringProperty();
        this.fathers = new ArrayList<>();
        this.file = null;
        this.type = type;
        switch (type) {
            case OBSERVATION:
                addObsContextMenu();
                break;
            case STEP:
                addStepContextMenu();
                break;
            case PROCESS:
                addProcessContextMenu();
                break;
            default:break;
        }
    }

    public ItineraryFile getItineraryFile() {
        return this.itineraryPart;
    }

    public void setItineraryFile(ItineraryFile itineraryPart) {
        this.itineraryPart = itineraryPart;
    }

    private void addObsContextMenu() {
        ContextMenu menu = new ContextMenu();
    }

    private void addProcessContextMenu() {

    }

    private void addStepContextMenu() {

    }

    public DataNode(String name, DataNodeType type) {
        this(type);
        this.name.setValue(name);
    }

    public DataNode(String name, GenericFile file, DataNodeType type) {
        this(name, type);
        this.file = file;
    }


    public void updateImage() {
        switch (this.type) {
            case PROCESS:
                if(((GeneralFile)file).isLoaded()) {
                    imageView.setImage(process_loaded);
                }
                else {
                    imageView.setImage(process);
                }
                break;
            case STEP:
                imageView.setImage(step);
                break;
            case OBSERVATION:
                imageView.setImage(obs);
                break;
            case ITINERARY:
                imageView.setImage(iti);
                break;
            default:
                break;
        }
    }


    public Node getGraphic() {
        updateImage();
        return imageView;
//        switch (this.type) {
//            case PROCESS:
//                if(((GeneralFile)file).isLoaded()) {
//                    imageView.setImage(process_loaded);
//                    return imageView;
//                }
//                else {
//                    imageView.setImage(process);
//                    return imageView;
//                }
//            case STEP:
//                imageView.setImage(step);
//                return imageView;
//            case OBSERVATION:
//                imageView.setImage(obs);
//                return imageView;
//            default:
//                    return null;
//        }
    }

    public GenericFile getFile() {
        return file;
    }

    public void addFather(DataNode father) {
        this.fathers.add(father);
    }

    public DataNodeType getType() {
        return this.type;
    }
    /**
     * @return
     */
    public ObservableList<DataNode> getSubNode() {

        try {
            FXCollections.sort(listSubNode);
        } catch (Exception e) {
            MainApp.logger.error(e.getClass() + " : " + e.getMessage() + " - impossible de trier la liste des fils", e);
        }
        return listSubNode;
    }

    public ObservableList<DataNode> getSubNodeNoSort() {
        return listSubNode;
    }

    public void addSubNode(DataNode node) {
        listSubNode.add(node);
    }

    public void addSubNode(List<DataNode> listNode) {
        listSubNode.addAll(listNode);
    }

    public void removeSubNode(DataNode node) {
        listSubNode.remove(node);
    }

    public void removeSubNode(List<DataNode> listNode) {
        listSubNode.removeAll(listNode);
    }

    public void clearSubNode() {
        listSubNode.forEach(DataNode::clearSubNode);
        for (DataNode n : listSubNode) {
            n.getFathers().remove(this);
        }
        listSubNode.clear();
    }
    /**
     * @return
     */
    public String getName() {
        return name.getValue();
    }



    public StringProperty nameProperty() {
        return name;
    }
    public StringProperty valueProperty() {
        return name;
    }
    public StringProperty textProperty() {
        return name;
    }


    /**
     * @param name
     */
    public void setName(String name) {
        this.name.set(name);
    }

    @Override
    public String toString() {
        return getName();
    }


    public List<DataNode> getFathers() {
        return fathers;
    }

    public void setFile(GenericFile file) {
        this.file = file;
    }


    @Override
    public int compareTo(DataNode o) {
        if(o == null) return 0;
        if(this.getType().equals(DataNodeType.STEP) && o.getType().equals(DataNodeType.STEP)) {
            // order by temporality
            StepFile current = (StepFile)this.getFile();
            StepFile otherStep = (StepFile)o.getFile();
            if(!current.isLeaf()) {
                current = current.getSubStep().stream().findFirst().orElse(null);
            }
            if(!otherStep.isLeaf()) {
                otherStep = otherStep.getSubStep().stream().findFirst().orElse(null);
            }
            if( current != null && otherStep != null && this.getItineraryFile() != null) {
                if(getItineraryFile().isLinked(current,otherStep)) {
                    return -1;
                } else {
                    if(getItineraryFile().isLinked(otherStep, current)) {
                        return 1;
                    }
                }
            }
        }
        if(this.getType().equals(DataNodeType.STEP) || this.getType().equals(DataNodeType.OBSERVATION)) {
            if (o.getType().equals(DataNodeType.STEP) || o.getType().equals(DataNodeType.OBSERVATION)) {
                String o1String = (this.getType().equals(DataNodeType.STEP) ? "aaa " : "ZZZ ") + this.getName().toLowerCase();
                String o2String = (o.getType().equals(DataNodeType.STEP) ? "aaa " : "ZZZ ") + o.getName().toLowerCase();
                return o1String.compareTo(o2String);
            }
        } else {
            if(this.getType().equals(DataNodeType.ITINERARY) && o.getType().equals(DataNodeType.ITINERARY)) {
                ItineraryFile i1 = (ItineraryFile) this.getFile();
                ItineraryFile i2 = (ItineraryFile) o.getFile();
                Integer ii1 = Integer.parseInt(i1.getItineraryNumber().replaceAll("\\D+",""));
                Integer ii2 = Integer.parseInt(i2.getItineraryNumber().replaceAll("\\D+",""));
                return ii1.compareTo(ii2);
            }
        }
        if(o != null) {
            return this.getName().toLowerCase().compareTo(o.getName().toLowerCase());
        }
        return 0;
    }
}
