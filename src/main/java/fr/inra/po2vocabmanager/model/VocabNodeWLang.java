/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.model;

import fr.inrae.po2engine.model.VocabConcept;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.ImageView;

public class VocabNodeWLang {
    private String lang;
    private String langLabel;
    private VocabConcept vocabNode;
    private ObservableValue<ImageView> flag;

    public VocabNodeWLang(String lang, String langLabel, ObservableValue<ImageView> flag, VocabConcept node) {
        this.flag = flag;
        this.lang = lang;
        this.vocabNode = node;
        this.langLabel = langLabel;
    }

    public String getLang() {
        return this.lang;
    }

    public ObservableValue<ImageView> getFlag() {
        return this.flag;
    }

    public VocabConcept getVocabNode() {
        return this.vocabNode;
    }

    public String getLangLabel() {
        return langLabel;
    }

}
