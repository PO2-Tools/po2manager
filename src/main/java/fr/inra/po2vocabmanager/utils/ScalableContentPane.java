/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.utils;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Region;
import javafx.scene.transform.Scale;

public class ScalableContentPane extends Region {

    // content property
    private final ObjectProperty<Node> content = new SimpleObjectProperty<>();
    public ObjectProperty<Node> contentProperty() { return content; }
    public Node getContent() { return content.get(); }
    public void setContent(Node content) { this.content.set(content); }
    private ImageView image;

    // aspect ratio proprety
    private final BooleanProperty keepAspectRatio = new SimpleBooleanProperty(true);
    public BooleanProperty keepAspectRatioProperty() { return keepAspectRatio; }
    public boolean isKeepAspectRatio() { return keepAspectRatio.get(); }
    public void setKeepAspectRatio(boolean keepAspectRatio) { this.keepAspectRatio.set(keepAspectRatio); }

    private final Scale contentScale = new Scale(1, 1, 0, 0);

    public ScalableContentPane() {
        this.image = new ImageView();
        this.content.addListener((obs, oldContent, newContent) -> {
            if(oldContent != null) {
                getChildren().clear();
                oldContent.getTransforms().remove(contentScale);
            }
            if(newContent != null) {
                newContent.getTransforms().add(contentScale);
                getChildren().setAll(newContent);
            }
        });
    }

    public ScalableContentPane(Node content) {
        this();
        this.content.set(content);
    }

    @Override
    protected void layoutChildren() {
        Node content = this.content.get();

        if(content == null) {
            return;
        }

        double contentWidth, contentHeight;
        if(content.getContentBias() == Orientation.HORIZONTAL) {
            contentWidth = content.prefWidth(-1);
            contentHeight = content.prefHeight(contentWidth);
        } else if(content.getContentBias() == Orientation.VERTICAL) {
            contentHeight = content.prefHeight(-1);
            contentWidth = content.prefWidth(contentHeight);
        } else { // null
            contentWidth = content.prefWidth(-1);
            contentHeight = content.prefHeight(-1);
        }

        double scaleX = getWidth() / contentWidth;
        double scaleY = getHeight() / contentHeight;

        double x, y;
        if(isKeepAspectRatio()) {
            double scale = Math.min(scaleX, scaleY);
            contentScale.setX(scale);
            contentScale.setY(scale);
            x = (getWidth() - scale * contentWidth) / 2;
            y = (getHeight() - scale * contentHeight) / 2;
        } else {
            contentScale.setX(scaleX);
            contentScale.setY(scaleY);
            x = 0;
            y = 0;
        }

        content.resizeRelocate(x, y, contentWidth, contentHeight);
    }

    @Override
    public Orientation getContentBias() {
        return content.get() != null
                ? content.get().getContentBias()
                : null;
    }

    @Override
    protected double computeMinWidth(double height) {
        return 1;
    }

    @Override
    protected double computeMinHeight(double width) {
        return 1;
    }

    @Override
    protected double computePrefWidth(double height) {
        return content.get() != null
                ? content.get().prefWidth(height)
                : 1;
    }

    @Override
    protected double computePrefHeight(double width) {
        return content.get() != null
                ? content.get().prefHeight(width)
                : 1;
    }

    @Override
    protected double computeMaxWidth(double height) {
        return Double.MAX_VALUE;
    }

    @Override
    protected double computeMaxHeight(double width) {
        return Double.MAX_VALUE;
    }

    public void setImage(Image image) {
        this.image.setImage(image);
        setContent(this.image);
    }

    public ImageView getImageView() {
        return this.image;
    }
}
