/*
 * Copyright (c) 2014-2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */
package fr.inra.po2vocabmanager.utils;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.collections.transformation.SortedList;
import javafx.scene.control.TreeItem;

/**
 * An extension of {@link TreeItem} with the possibility to sort its children. To enable sorting
 * it is necessary to set the {@link TreeItemComparator}. If no comparator is set, then
 * the tree item will attempt so bind itself to the comparator of its parent.
 *
 * @param <T> The type of the {@link #getValue() value} property within {@link TreeItem}.
 */
public class SortableTreeItem<T> extends FilterableTreeItem<T> {
    private SortedList<TreeItem<T>> sortedList;

    private ObjectProperty<TreeItemComparator<T>> comparator;

    /**
     * Creates a new {@link TreeItem} with sorted children. To enable sorting it is
     * necessary to set the {@link TreeItemComparator}. If no comparator is set, then
     * the tree item will attempt so bind itself to the comparator of its parent.
     *
     * @param value the value of the {@link TreeItem}
     */
    public SortableTreeItem(T value) {
        super(value);
        parentProperty().addListener((o, oV, nV) -> {
            if (nV != null && nV instanceof SortableTreeItem && getComparator() == null) {
                comparatorProperty().bind(((SortableTreeItem<T>) nV).comparatorProperty());
            }
        });
    }

    @Override
    protected ObservableList<TreeItem<T>> getBackingList() {
        if (this.sortedList == null) {
            this.sortedList = new SortedList<>(super.getBackingList());
            this.sortedList.comparatorProperty().bind(Bindings.createObjectBinding(() -> {
                if (getComparator() == null) {
                    return null;
                }
                return  (o1, o2) -> getComparator().compare(this, o1.getValue(), o2.getValue());
            }, comparatorProperty()));
        }
        return this.sortedList;
    }

    /**
     * @return the comparator property
     */
    public final ObjectProperty<TreeItemComparator<T>> comparatorProperty() {
        if (this.comparator == null) {
            this.comparator = new SimpleObjectProperty<>();
        }
        return this.comparator;
    }

    /**
     * @return the comparator
     */
    public final TreeItemComparator<T> getComparator() {
        return comparatorProperty().get();
    }

    /**
     * Set the comparator
     * @param comparator the comparator
     */
    public final void setComparator(TreeItemComparator<T> comparator) {
        comparatorProperty().set(comparator);
    }
}