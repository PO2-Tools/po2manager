/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.utils;

import fr.inra.po2vocabmanager.MainApp;
import fr.inrae.po2engine.externalTools.CloudConnector;
import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.effect.ColorAdjust;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class StatusBox {
    private ArrayList<Node> listNodes = new ArrayList<>();
    private Integer spacing = 5;
    private Integer height = 70;

    public StatusBox(MainApp mainApp) {
        ColorAdjust grayScale = new ColorAdjust();
        grayScale.setSaturation(-1);
        ColorAdjust normalScale = new ColorAdjust();

        ////////// connexion part
        VBox boxConnect = new VBox();
        boxConnect.setPrefWidth(100);
        boxConnect.setAlignment(Pos.TOP_CENTER);
        boxConnect.setSpacing(10);
        Label labelOnline = new Label();
        labelOnline.setStyle("-fx-font-weight: bold");
        ImageView onlineImage = new ImageView(UITools.getImage("resources/images/globe.png", height, height,true, true));
        onlineImage.setPreserveRatio(true);
        onlineImage.effectProperty().bind(Bindings.when(CloudConnector.onlineProperty()).then(normalScale).otherwise(grayScale));
        labelOnline.textProperty().bind(Bindings.when(CloudConnector.onlineProperty()).then("online").otherwise("offline"));
        boxConnect.getChildren().addAll(labelOnline, onlineImage);

        listNodes.add(boxConnect);


        // onto part
        Image readMode = UITools.getImage("resources/images/read-overlay.png", height,height, true, true);
        Image writeMode = UITools.getImage("resources/images/write-overlay.png", height,height, true, true);

        Image syncOK = UITools.getImage("resources/images/synchroOK-overlay.png", height,height, true, true);
        Image syncKO = UITools.getImage("resources/images/synchroKO-overlay.png", height,height, true, true);


        ImageView readWriteModeOnto = new ImageView();
        readWriteModeOnto.setPreserveRatio(true);
        readWriteModeOnto.imageProperty().bind(Bindings.when(mainApp.getOntologyControler().getCanEditProperty()).then(writeMode).otherwise(readMode));
        ImageView readWriteModeData = new ImageView();
        readWriteModeData.setPreserveRatio(true);
        readWriteModeData.imageProperty().bind(Bindings.when(mainApp.getDataControler().getCanEditProperty()).then(writeMode).otherwise(readMode));

        ImageView syncOnto = new ImageView();
        syncOnto.setPreserveRatio(true);
        syncOnto.imageProperty().bind(Bindings.when(mainApp.getOntologyControler().getSyncCloudProperty()).then(syncOK).otherwise(syncKO));
        ImageView syncData = new ImageView();
        syncData.setPreserveRatio(true);
        syncData.imageProperty().bind(Bindings.when(mainApp.getDataControler().getSyncCloudProperty()).then(syncOK).otherwise(syncKO));


        Label labelOnto = new Label();
        labelOnto.setStyle("-fx-font-weight: bold");
        labelOnto.setWrapText(true);
        labelOnto.textProperty().bind(Bindings.when(mainApp.getOntologyControler().getFileName().isEmpty()).then("No ontology").otherwise(mainApp.getOntologyControler().getFileName()));


        Group overlayOnto = new Group();
        ImageView ontoImage = new ImageView(UITools.getImage("resources/images/ontology.png", height,height,true, true));
        ontoImage.setPreserveRatio(true);
        overlayOnto.effectProperty().bind(Bindings.when(mainApp.getOntologyControler().getFileName().isEmpty()).then(grayScale).otherwise(normalScale));
        overlayOnto.getChildren().addAll(ontoImage, readWriteModeOnto, syncOnto);

        ToggleButton buttonOnto = new ToggleButton();
        buttonOnto.setGraphic(overlayOnto);

        buttonOnto.setOnMouseClicked(e->mainApp.showOntologyMod());
        buttonOnto.setPrefWidth(80);
        buttonOnto.setAlignment(Pos.TOP_CENTER);
        buttonOnto.selectedProperty().bindBidirectional(mainApp.onOntologyViewProperty());

        VBox boxOnto = new VBox();
        boxOnto.setPrefWidth(80);
        boxOnto.setAlignment(Pos.TOP_CENTER);
        boxOnto.setSpacing(5);
        boxOnto.getChildren().addAll(labelOnto, buttonOnto);

        listNodes.add(boxOnto);

        // data part

        Label labelData = new Label();
        labelData.setStyle("-fx-font-weight: bold");
        labelData.setWrapText(true);
        labelData.textProperty().bind(Bindings.when(mainApp.getDataControler().getFileName().isEmpty()).then("No data").otherwise(mainApp.getDataControler().getFileName()));


        Group overlayData = new Group();
        ImageView dataImage = new ImageView(UITools.getImage("resources/images/data-folders.png", height,height,true, true));
        dataImage.setPreserveRatio(true);
        overlayData.effectProperty().bind(Bindings.when(mainApp.getDataControler().getFileName().isEmpty()).then(grayScale).otherwise(normalScale));
        overlayData.getChildren().addAll(dataImage, readWriteModeData, syncData);

        ToggleButton buttonData = new ToggleButton();
        buttonData.setGraphic(overlayData);

        buttonData.setOnMouseClicked(e->mainApp.showDataMod());
        buttonData.setPrefWidth(80);
        buttonData.setAlignment(Pos.TOP_CENTER);
        buttonData.selectedProperty().bindBidirectional(mainApp.onDataViewProperty());

        VBox boxData = new VBox();
        boxData.setPrefWidth(80);
        boxData.setAlignment(Pos.TOP_CENTER);
        boxData.setSpacing(5);
        boxData.getChildren().addAll(labelData, buttonData);


        listNodes.add(boxData);
    }

    public ArrayList<Node> getListNodes() {
        return listNodes;
    }

    public Integer getSpacing() {
        return spacing;
    }
}
