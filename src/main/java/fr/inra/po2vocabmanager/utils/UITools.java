/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.utils;

import fr.inra.po2vocabmanager.MainApp;
import fr.inrae.po2engine.externalTools.CloudConnector;
import fr.inrae.po2engine.model.*;
import fr.inrae.po2engine.model.dataModel.KeyWords;
import fr.inrae.po2engine.model.partModel.ComplexTable;
import fr.inrae.po2engine.model.partModel.GenericTable;
import fr.inrae.po2engine.model.partModel.SimpleTable;
import fr.inrae.po2engine.utils.FieldState;
import fr.inrae.po2engine.utils.ProgressPO2;
import fr.inrae.po2engine.utils.Tools;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.BooleanBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.collections.transformation.FilteredList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import javafx.util.Pair;
import javafx.util.StringConverter;
import org.apache.logging.log4j.LogManager;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class UITools {

    private static Stage progressStage;
    private static VBox listVBox;
    private static HashMap<ProgressPO2, VBox> listProgress;
    private static org.apache.logging.log4j.Logger logger = LogManager.getLogger(UITools.class);

    private UITools() {}

//    public static void addPopOver(TextField text, String... topURIs) {
//        if(listListener.containsKey(text)) {
//            text.focusedProperty().removeListener(listListener.get(text));
//            listListener.remove(text);
//        }
//        if(!listListener.containsKey(text)) {
//            VBox box = new VBox();
//            TextField filter = new TextField();
//            Label l = new Label("Filter :");
//            TreeView<VocabConcept> tree = new TreeView<>();
//            box.getChildren().addAll(l, filter, tree);
//
//            VocabConcept frv = new VocabConcept("", null);
//            frv.setName("fake-root");
//            FilterableTreeItem<VocabConcept> fakeRoot = new FilterableTreeItem<>(frv);
//
//            tree.setShowRoot(false);
//
//
//            PopOver pop = new PopOver(box);
//            tree.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
//                if (newValue != null) {
//                    text.setText(newValue.getValue().getLabel("en").get());
//                    pop.hide();
//                }
//            });
//            ChangeListener<Boolean> listener = new ChangeListener<Boolean>() {
//                @Override
//                public void changed(ObservableValue<? extends Boolean> observableValue, Boolean old, Boolean neww) {
//                    if (text.isEditable() && !text.isDisable() && MainApp.getOntologyControler() != null && MainApp.getOntologyControler().getCurrentOntology() != null) {
//                        if (neww) {
//                            fakeRoot.getChildren().clear();
//
//                            for(String topURI : topURIs) {
//                                // recherche du root item
//                                TreeItem<VocabConcept> localRoot = MainApp.getOntologyControler().getRootItem().getChildren().stream().filter(vocabConceptTreeItem -> {
//                                    return vocabConceptTreeItem.getValue().getURI().equalsIgnoreCase(topURI);
//                                }).findFirst().orElse(null);
//                                FilterableTreeItem<VocabConcept> filterLocalRoot = new FilterableTreeItem<>(localRoot.getValue());
//
//                                buildTree(filterLocalRoot, localRoot.getValue());
//
//                                filterLocalRoot.predicateProperty().bind(Bindings.createObjectBinding(() -> {
//                                    if (filter.getText() == null || filter.getText().isEmpty())
//                                        return TreeItemPredicate.create(value -> true);
//                                    return TreeItemPredicate.create(value -> value.toString().toLowerCase().contains(filter.getText().toLowerCase()));
//                                }, filter.textProperty()));
//                                fakeRoot.getChildren().add(filterLocalRoot);
//                            }
//                            tree.setRoot(fakeRoot);
//                            pop.show(text);
//                        }
//                    } else {
//                        pop.hide();
//                    }
//                }
//
//                public void buildTree(FilterableTreeItem<VocabConcept> filterLocalRoot, VocabConcept localRoot) {
//                    for(VocabConcept son : localRoot.getSubNode().filtered(vocabConcept -> !vocabConcept.getDeprecated())) {
//                        FilterableTreeItem<VocabConcept> vson = new FilterableTreeItem<>(son);
//                        filterLocalRoot.getInternalChildren().add(vson);
//                        buildTree(vson, son);
//                    }
//                }
//            };
//            text.focusedProperty().addListener(listener);
//            listListener.put(text, listener);
//        }
//    }
    public static Image getImage(String resource, double width, double height, boolean ratio, boolean smooth) {
        String resourceAsString = getResourcesAsString(resource);
        return resourceAsString!=null?new Image(resourceAsString, width, height,ratio, smooth):null;
    }

    public static Image getImage(String resource) {
        String resourceAsString = getResourcesAsString(resource);
        return resourceAsString!=null?new Image(resourceAsString):null;
    }

    public static String getResourcesAsString(String resource) {
        URL u = MainApp.class.getClassLoader().getResource(resource);
        return u!=null?u.toString():null;

    }
    public static AutoCompletionBinding addAutoComplete(TextField text, ObservableList<VocabConcept> list) {
//        addPopOver(text, "t");
//        text.setRight(new ImageView(UITools.getImage("resources/images/drop-down-arrow.png")));
        text.textProperty().unbind();
//        text.styleProperty().unbind();
        FilteredList<VocabConcept> filteredList = new FilteredList<>(list, p -> true);
        AutoCompletionBinding ab = TextFields.bindAutoCompletion(text, iSuggestionRequest -> {
            if (!text.getText().isEmpty()) {
                filteredList.setPredicate(item -> item.containsLabelOrSynonym(text.getText()));
            } else {
                filteredList.setPredicate(item -> true);
            }
            return filteredList;
        });
        return ab;
    }

    public static FXMLLoader getFXMLLoader(String resource) {
        // first checking for contextclassLoader (macos bug if Thread is AppKit Thread)
        if (Thread.currentThread().getContextClassLoader() == null) {
            Thread.currentThread().setContextClassLoader(UITools.class.getClassLoader());
        }
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(MainApp.class.getResource(resource));
        return loader;
    }

    public static ObservableValue<ImageView> getFlag(String lang) {

        Image i;
        try {
            i = UITools.getImage("resources/images/flags/" + lang.toLowerCase() + ".png");
            if (i == null) {
                i = UITools.getImage("resources/images/flags/unknown.png");
            }
        } catch (NullPointerException e) {
            MainApp.logger.error("flag for " + lang.toLowerCase());
            i = UITools.getImage("resources/images/flags/unknown.png");
        }
        ImageView img = new ImageView(i);
        Tooltip.install(img, new Tooltip("test"));
        return new SimpleObjectProperty<>(img);
    }

    public static void initProgressBar() {
        Tools.initProgress();
        progressStage = new Stage();
        progressStage.setMinWidth(300);
        listVBox = new VBox(5);
        listProgress = new HashMap<>();
        progressStage.setScene(new Scene(listVBox, 300, 80));
        progressStage.initOwner(MainApp.primaryStage);
        progressStage.initModality(Modality.APPLICATION_MODAL);
        progressStage.minHeightProperty().bind(Bindings.size(listVBox.getChildren()).multiply(80) );
        progressStage.maxHeightProperty().bind(Bindings.size(listVBox.getChildren()).multiply(80) );
        final Window window = progressStage.getScene().getWindow();



        window.addEventHandler(WindowEvent.WINDOW_SHOWN, new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                window.setX(MainApp.primaryStage.getX() + MainApp.primaryStage.getWidth() / 2 - progressStage.getWidth() / 2);
                window.setY(MainApp.primaryStage.getY() + MainApp.primaryStage.getHeight() / 2 - progressStage.getHeight() / 2);
            }
        });

        Tools.getListProgress().addListener((MapChangeListener<? super String, ? super ProgressPO2>) change -> {
            if(change.wasAdded()) {
                final VBox box = new VBox();
                Label progressMessage = new Label();
                box.setAlignment(Pos.CENTER);
                box.setSpacing(5.0);
                box.setMinWidth(300);
                box.getChildren().add(progressMessage);
                ProgressBar progressBar = new ProgressBar();
                progressBar.setPrefWidth(280.0);
                box.getChildren().add(progressBar);
                progressMessage.setText(change.getValueAdded().getText());
                progressBar.setProgress(change.getValueAdded().getProgress());
                change.getValueAdded().textProperty().addListener((observableValue, s, t1) -> {
                    Platform.runLater(() -> {
                        progressMessage.setText(t1);
                    });
                });
                change.getValueAdded().progressProperty().addListener((observableValue, s, t1) -> {
                    Platform.runLater(() -> {
                        progressBar.setProgress(t1.doubleValue());
                    });
                });

                listProgress.put(change.getValueAdded(), box);
                Platform.runLater(() -> {
                    listVBox.getChildren().add(listProgress.get(change.getValueAdded()));
                });
            }
            if(change.wasRemoved()) {
                VBox boxRemove = listProgress.get(change.getValueRemoved());
                Platform.runLater(() -> {
                    listVBox.getChildren().remove(boxRemove);
                });
            }
            if(Tools.getListProgress().isEmpty()) {
                Platform.runLater(() -> {
                    progressStage.hide();
                });
            } else {
                Platform.runLater(() -> {
                    progressStage.show();
                });
            }
        });

       // progressStage.show();
    }

    public static void showUnitUcumPrefix() {
        Dialog infoPrefix = new Dialog();
        infoPrefix.setTitle("prefix UCUM code");
        infoPrefix.initOwner(MainApp.primaryStage);
        infoPrefix.initModality(Modality.NONE);
        infoPrefix.getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
        infoPrefix.setResizable(true);
        GridPane panePref = new GridPane();

        infoPrefix.getDialogPane().setContent(panePref);

//                                                ComboBox<String> comboPrefix = new ComboBox<>();
        Integer prefLine = 0;
        Integer prefCol = 0;
        for(Map.Entry<String, String> entry : Tools.prefixName.entrySet()) {
            TextField ll = new TextField(entry.getKey() + " --> " + entry.getValue());
            ll.setEditable(false);
            panePref.add(ll, prefCol, prefLine);
            prefCol++;
            if(prefCol > 3) {
                prefCol = 0;
                prefLine++;
            }
//                                                    comboPrefix.getItems().add(entry.getKey() + " -- > " + entry.getValue());
        }
        infoPrefix.show();
    }

    public static void showEditUnit(Button textUnit) {

        Dialog<ButtonType> editValue = new Dialog();
        editValue.setResizable(true);
        editValue.setTitle("Edit unit");
        editValue.initModality(Modality.APPLICATION_MODAL);
        editValue.initOwner(textUnit.getScene().getWindow());
        editValue.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
//                                                editValue.getDialogPane().setPrefSize(400, 150);
        TextField unitValue = new TextField();
        StringProperty printUnitValue = new SimpleStringProperty("none");
        unitValue.textProperty().addListener((observableValue, oldValue, newValue) -> {
            String prettyValueD = Tools.getPrettyUnit(newValue);
            if(prettyValueD != null) {
                printUnitValue.setValue(prettyValueD);
            } else {
                printUnitValue.setValue("Not a valid unit");
            }
        } );

        unitValue.setText(textUnit.getTooltip().getText());

        Label labPrintUnit = new Label();
        labPrintUnit.textProperty().bind(Bindings.concat("Unit : ", printUnitValue));
        GridPane paneShow = new GridPane();
        ColumnConstraints c1 = new ColumnConstraints();
        c1.setPercentWidth(35);
        paneShow.getColumnConstraints().addAll(c1);

        Button butShowPref = new Button("Show prefix code");
        butShowPref.setOnMouseClicked(mouseEvent -> {
            UITools.showUnitUcumPrefix();
        });
        Button butShowUnit = new Button("Show units code");
        butShowUnit.setOnMouseClicked(mouseEvent -> {
            UITools.showUnitUcumCode();
        });

        FlowPane p = new FlowPane(butShowPref, butShowUnit);
        paneShow.add(labPrintUnit, 0,0);
        paneShow.add(p, 1,0);

        VBox b = new VBox(4.0);
        b.getChildren().addAll(unitValue, paneShow);
        editValue.getDialogPane().setContent(b);
        Hyperlink linkToUCUM = new Hyperlink();
        linkToUCUM.setText("here");
        linkToUCUM.setOnAction(actionEvent -> MainApp.openBrowser("http://unitsofmeasure.org/ucum.html"));
        Hyperlink linkToUcumSem = new Hyperlink();
        linkToUcumSem.setText("here");
        linkToUcumSem.setOnAction(actionEvent -> MainApp.openBrowser("https://finto.fi/ucum/en/"));

        TextFlow unitInfo = new TextFlow();
        unitInfo.getChildren().add(new Text("Only Ucum code available "));
        unitInfo.getChildren().add(linkToUCUM);
        unitInfo.getChildren().add(new Text(" or "));
        unitInfo.getChildren().add(linkToUcumSem);
        unitInfo.getChildren().add(new Text(" can be used in the text field.\n" +
                "To choose a prefix, click on \"Show prefix code\"\n" +
                "   e.g. prefix for \"micro\" is u\n\n" +
                "To display the existing codes, click on \"Show units code\"\n" +
                "   e.g. code for \"degree Celsius\" is Cel\n\n" +
                "Exponents are written directly as numbers following the unit.\n" +
                "   e.g. m2 for square meter\n" +
                "        m3 for cubic meter\n\n" +
                "Care must also be taken to combine units correctly\n" +
                "   e.g. m/s or m.s-1 are valid entries.\n\n" +
                "If there is no entry in the field \"unit\", then the value is typed as a string of characters (qualitative value).\n\n" +
                "For numeric values that have no unit or dimensionless numbers, use an annotation between curly braces (e.g. {dimensionless} or {one}).\nThis entry will be interpreted as a vector of dimension 1.\n\n" +
                "If you want to add any details, you can annotate your unit with curly braces { }\n" +
                "   e.g.  kg{total} --> kg(total) will be displayed in the field \"unit\"\n\n" +
                "Also, please use curly braces if your unit is not a valid SI unit, or if you do not wish any conversion.\n" +
                "There must be no spaces in the content entered between curly braces."));

        editValue.getDialogPane().setExpandableContent(unitInfo);

        Optional<ButtonType> result = editValue.showAndWait();
        if (result.get() == ButtonType.OK) {
            String prettyValueD = Tools.getPrettyUnit(unitValue.getText());
            if(prettyValueD != null) {
                textUnit.setStyle(" -fx-text-fill: black;  -fx-opacity: 1;");
                textUnit.getTooltip().setText(unitValue.getText());
                textUnit.setText(prettyValueD);
            } else {
                textUnit.setStyle("-fx-table-cell-border-color: red; -fx-text-fill: black; -fx-border-color: red; -fx-opacity: 1;");
                textUnit.getTooltip().setText(unitValue.getText());
                textUnit.setText(unitValue.getText());
            }
            //todo a changer. Il ne faut pas faire comme ca.
            MainApp.getDataControler().getCurrentData().setModified(true);
        }

    }

    public static void showUnitUcumCode() {
        Dialog infoUnit = new Dialog();
        infoUnit.setTitle("UCUM code");
        infoUnit.initOwner(MainApp.primaryStage);
        infoUnit.initModality(Modality.NONE);
        infoUnit.getDialogPane().getButtonTypes().addAll(ButtonType.CLOSE);
        infoUnit.setResizable(true);
        ArrayList<TextField> listText = new ArrayList<>();

        BorderPane bpane = new BorderPane();
        FlowPane paneUnit = new FlowPane(Orientation.HORIZONTAL);

        VBox vb = new VBox(5);
        Label ls = new Label("Search : ");
        TextField tf = new TextField();
        tf.textProperty().addListener((observableValue, s, t1) -> {
            paneUnit.getChildren().clear();
            if(t1 == null || t1.isEmpty()) {
                paneUnit.getChildren().addAll(listText);
            } else {
                listText.stream().filter(textField -> textField.getText().toLowerCase().contains(t1.toLowerCase())).forEach(textField->paneUnit.getChildren().add(textField));
            }
        });
        vb.getChildren().addAll(ls, tf);

        paneUnit.prefWrapLengthProperty().bind(infoUnit.widthProperty());
//                                GridPane paneUnit = new GridPane();

        ScrollPane scrol = new ScrollPane(paneUnit);
        bpane.setTop(vb);
        bpane.setCenter(scrol);

        infoUnit.getDialogPane().setContent(bpane);
        infoUnit.getDialogPane().setPrefHeight(500);
        infoUnit.getDialogPane().setPrefWidth(900);

//                                                ComboBox<String> comboCode = new ComboBox<>();
        List<Map.Entry<String, String>> list = new ArrayList<>(Tools.unitName.entrySet());
        list.sort(Map.Entry.comparingByValue());

        for (Map.Entry<String, String> entry : list) {
            TextField ll = new TextField(entry.getKey() + " --> " + entry.getValue());
            ll.setEditable(false);
            paneUnit.getChildren().add(ll);
            listText.add(ll);
        }

        infoUnit.show();
    }

    public static Boolean startLogin() {
        CloudConnector.checkOnline();

        if (CloudConnector.onlineProperty().get() && !CloudConnector.isInitUser()) {
            Dialog<Pair<String, String>> login = new Dialog<>();
            login.setResizable(true);
            login.setTitle("Account login");
            login.setHeaderText("Please enter your login and password");
            login.setGraphic(new ImageView(UITools.getImage("resources/images/login-icon.png")));

            GridPane grid = new GridPane();
            grid.setHgap(10);
            grid.setVgap(10);
            grid.setPadding(new Insets(20, 150, 10, 10));

            TextField username = new TextField();
            username.setPromptText("Username");
            Platform.runLater(() -> username.requestFocus());
            PasswordField password = new PasswordField();
            password.setPromptText("Password");

            grid.add(new Label("Username:"), 0, 0);
            grid.add(username, 1, 0);
            grid.add(new Label("Password:"), 0, 1);
            grid.add(password, 1, 1);

            login.getDialogPane().setContent(grid);

            ButtonType loginButton = new ButtonType("Login", ButtonBar.ButtonData.OK_DONE);
            login.getDialogPane().getButtonTypes().addAll(loginButton, ButtonType.CANCEL);
            login.getDialogPane().lookupButton(loginButton).addEventFilter(
                    ActionEvent.ACTION,
                    event -> {
                        event.consume();
                        login.setGraphic(new ProgressIndicator());
                        login.setHeaderText("Authentication in progress ...");
                        password.setDisable(true);
                        username.setDisable(true);
                        login.getDialogPane().lookupButton(loginButton).setDisable(true);
                        login.getDialogPane().lookupButton(ButtonType.CANCEL).setDisable(true);
                        Task<Boolean> task = new Task<Boolean>() {
                            @Override
                            protected Boolean call() throws Exception {
                                return CloudConnector.initUser(username.getText(), password.getText());
                            }
                        };
                        task.setOnSucceeded(event1 -> {
                            if (!CloudConnector.isInitUser()) {
                                login.setGraphic(new ImageView(UITools.getImage("resources/images/error.png")));
                                login.setHeaderText("Bad login or password");
                                username.setText("");
                                password.setText("");
                                password.setDisable(false);
                                username.setDisable(false);
                                login.getDialogPane().lookupButton(loginButton).setDisable(false);
                                login.getDialogPane().lookupButton(ButtonType.CANCEL).setDisable(false);
                            } else {
                                login.setGraphic(new ImageView(UITools.getImage("resources/images/valid.png")));
                                login.setHeaderText("Your are now connected");
                                Timeline idle = new Timeline(new KeyFrame(Duration.seconds(2), new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        login.close();
                                    }
                                }));
                                idle.setCycleCount(1);
                                idle.play();
                            }
                        });

                        new Thread(task).start();
                    });

            login.initOwner(MainApp.primaryStage);
            login.getDialogPane().setMinSize(400, 200);
            Optional<Pair<String, String>> result = login.showAndWait();
        }

        return CloudConnector.isInitUser();
    }

    public static Node getNode(ComplexField cField) {

        GridPane grid = new GridPane();
        Tooltip tool = new Tooltip();
        tool.textProperty().bind(cField.getTooltip());
        Tooltip.install(grid, tool);
        Label labFirst = new Label();
        labFirst.textProperty().bind(Bindings.when(cField.getUnit().isEmpty()).then(cField.getValue()).otherwise(Bindings.concat(cField.getValue(), " (",Tools.getPrettyUnit(cField.getUnit().getValue()),")")));
        grid.add(labFirst, 0,0);

        Label labSecond = new Label();
        labSecond.setText(cField.getListObject().stream().collect(Collectors.joining("::")));
        grid.add(labSecond, 0,1);

        return grid;
    }

    public static void simpleBindId(ComplexField cField, TextField tField) {
        cField.getID().unbind();
        tField.setText(cField.getID().getValue());
        cField.getID().bind(tField.textProperty());
        tField.styleProperty().unbind();
        tField.styleProperty().bind(Bindings.when(cField.stateProperty().isEqualTo(FieldState.WARNING)).then("-fx-border-color: orange;").otherwise(Bindings.when(cField.stateProperty().isEqualTo(FieldState.ERROR)).then("-fx-border-color: red;").otherwise("")));
    }
    public static void simpleBindValue(ComplexField cField, TextField tField) {
        cField.unbindReplicatesValues();
        tField.setText(cField.getValue().getValue());
        cField.getValue().bind(tField.textProperty());
        tField.styleProperty().unbind();
        tField.styleProperty().bind(Bindings.when(cField.stateProperty().isEqualTo(FieldState.WARNING)).then("-fx-border-color: orange;").otherwise(Bindings.when(cField.stateProperty().isEqualTo(FieldState.ERROR)).then("-fx-border-color: red;").otherwise("")));
    }

    public static void simpleBindValue(ComplexField cField, Replicate replicate, TextField tField) {
        cField.unbindReplicatesValues();
        int replicateID = replicate != null ? replicate.getId() : 0;
        tField.setText(cField.getValueWithReplicate(replicateID).getValue());
        cField.getValueWithReplicate(replicateID).bind(tField.textProperty());
        tField.styleProperty().unbind();
        if(replicate == null) {
            tField.styleProperty().bind(Bindings.when(cField.stateProperty().isEqualTo(FieldState.WARNING)).then("-fx-border-color: orange;").otherwise(Bindings.when(cField.stateProperty().isEqualTo(FieldState.ERROR)).then("-fx-border-color: red;").otherwise("")));
        } else {
            tField.styleProperty().bind(Bindings.when(cField.stateProperty().isEqualTo(FieldState.WARNING)).then("-fx-border-color: orange;").otherwise(Bindings.when(cField.stateProperty().isEqualTo(FieldState.ERROR)).then("-fx-border-color: red;").otherwise("-fx-border-color: DarkSlateBlue;")));
        }
    }

    public static void simpleBindValue(ComplexField cField, TextArea tField) {
        cField.getValue().unbind();
        tField.setText(cField.getValue().getValue());
        cField.getValue().bind(tField.textProperty());
        tField.styleProperty().unbind();
        tField.styleProperty().bind(Bindings.when(cField.stateProperty().isEqualTo(FieldState.WARNING)).then("-fx-border-color: orange;").otherwise(Bindings.when(cField.stateProperty().isEqualTo(FieldState.ERROR)).then("-fx-border-color: red;").otherwise("")));
    }

    public static void bindComboBoxAgent(ObservableMap<Integer, ObjectProperty<HashMap<KeyWords, ComplexField>>> agentProp, ObservableList<HashMap<KeyWords, ComplexField>> list, Replicate replicate, ComboBox<HashMap<KeyWords, ComplexField>> comboField) {
        int replicateID = replicate != null ? replicate.getId() : 0;
        comboField.setConverter(new StringConverter<HashMap<KeyWords, ComplexField>>() {
            @Override
            public String toString(HashMap<KeyWords, ComplexField> keyWordsComplexFieldHashMap) {
                if (keyWordsComplexFieldHashMap == null){
                    return null;
                } else {
                    return Tools.agentToString(keyWordsComplexFieldHashMap);
                }
            }

            @Override
            public HashMap<KeyWords, ComplexField> fromString(String s) {
                HashMap<KeyWords, ComplexField> val = list.stream().filter(keyWordsComplexFieldHashMap -> {
                    return s.equalsIgnoreCase(Tools.agentToString(keyWordsComplexFieldHashMap));
                }).findFirst().orElse(null);
                return val;
            }
        });
        comboField.setCellFactory(p -> {
            return new ListCell<HashMap<KeyWords, ComplexField>>() {

                @Override
                protected void updateItem(HashMap<KeyWords, ComplexField> item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setGraphic(null);
                        setText("");
                    } else {
                        setGraphic(null);
                        setText(Tools.agentToString(item));
                    }
                }
            };
        });

        comboField.setValue(null);
        ObservableList<HashMap<KeyWords, ComplexField>> listCopy = FXCollections.observableArrayList();
        listCopy.add(Tools.createFakeAgentFromString(""));

        agentProp.computeIfAbsent(replicateID, k -> {
            SimpleObjectProperty sop = new SimpleObjectProperty<>();
            sop.addListener(Data.changeListener);
            return sop;
        } );

        if(!list.contains(agentProp.get(replicateID).getValue())) {
            if(!Tools.agentToString(agentProp.get(replicateID).getValue()).isBlank()) {
                listCopy.add(agentProp.get(replicateID).getValue());
            }
        }
        listCopy.addAll(list);
        comboField.setItems(listCopy);
        comboField.setValue(agentProp.get(replicateID).getValue());
        agentProp.get(replicateID).bind(comboField.valueProperty());

        comboField.styleProperty().unbind();
        BooleanBinding containAgent = Bindings.createBooleanBinding( () -> list.contains( agentProp.get(replicateID).getValue() ) || Tools.agentToString(agentProp.get(replicateID).getValue()).isEmpty(), list, agentProp );
        comboField.styleProperty().bind(Bindings.when(containAgent).then("-fx-border-color: DarkSlateBlue;").otherwise("-fx-border-color: red;"));
    }

    public static void bindComboBoxAgent(ObjectProperty<HashMap<KeyWords, ComplexField>> agentProp, ObservableList<HashMap<KeyWords, ComplexField>> list, ComboBox<HashMap<KeyWords, ComplexField>> comboField) {
        comboField.setConverter(new StringConverter<HashMap<KeyWords, ComplexField>>() {
            @Override
            public String toString(HashMap<KeyWords, ComplexField> keyWordsComplexFieldHashMap) {
                if (keyWordsComplexFieldHashMap == null){
                    return null;
                } else {
                    return Tools.agentToString(keyWordsComplexFieldHashMap);
                }
            }

            @Override
            public HashMap<KeyWords, ComplexField> fromString(String s) {
                HashMap<KeyWords, ComplexField> val = list.stream().filter(keyWordsComplexFieldHashMap -> {
                    return s.equalsIgnoreCase(Tools.agentToString(keyWordsComplexFieldHashMap));
                }).findFirst().orElse(null);
                return val;
            }
        });
        comboField.setCellFactory(p -> {
            return new ListCell<HashMap<KeyWords, ComplexField>>() {

                @Override
                protected void updateItem(HashMap<KeyWords, ComplexField> item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item == null || empty) {
                        setGraphic(null);
                        setText("");
                    } else {
                        setGraphic(null);
                        setText(Tools.agentToString(item));
                    }
                }
            };
        });

        comboField.setValue(null);
        ObservableList<HashMap<KeyWords, ComplexField>> listCopy = FXCollections.observableArrayList();
        listCopy.add(Tools.createFakeAgentFromString(""));
        if(!list.contains(agentProp.getValue())) {
            if(!Tools.agentToString(agentProp.getValue()).isBlank()) {
                listCopy.add(agentProp.getValue());
            }
        }
        listCopy.addAll(list);
        comboField.setItems(listCopy);
        comboField.setValue(agentProp.getValue());
        agentProp.bind(comboField.valueProperty());

        comboField.styleProperty().unbind();
        BooleanBinding containAgent = Bindings.createBooleanBinding( () -> list.contains( agentProp.getValue() ) || Tools.agentToString(agentProp.getValue()).isEmpty(), list, agentProp );
        comboField.styleProperty().bind(Bindings.when(containAgent).then("").otherwise("-fx-border-color: red;"));
    }

    public static void bindTable(GenericTable table, Replicate replicate,  TableView tableView) {
        if(table instanceof SimpleTable) {
            bindSimpleTable((SimpleTable) table, replicate, tableView);
        }
        if(table instanceof ComplexTable) {
            bindComplexTable((ComplexTable) table, replicate, tableView);
        }
    }

    public static void bindComplexTable(ComplexTable complexTable, Replicate replicate, TableView tableView) {
        ChangeListener changeListener = (observable, oldValue, newValue) -> {
            complexTable.getData().setModified(true);
        };
        tableView.getColumns().clear();
        tableView.setTableMenuButtonVisible(true);
        int replicateID = replicate != null ? replicate.getId() : 0;

        Button addLine = new Button("New line ");
        addLine.disableProperty().bind(tableView.editableProperty().not());
        addLine.setGraphic(new ImageView(UITools.getImage("resources/images/add_row-16.png")));
        addLine.setOnAction(actionEvent -> {
            complexTable.addLine(-1);
        });
        Label noContent = new Label("No content in table");
        noContent.setGraphic(addLine);
        noContent.setContentDisplay(ContentDisplay.BOTTOM);
        tableView.setPlaceholder(noContent);

        TableColumn<Map<ComplexField, ComplexField>, ComplexField> colAction;
        // on créer les colonnes
        TableColumn<Map<ComplexField, ComplexField>, ComplexField> colNum = new TableColumn("#");
        colNum.setMinWidth(40);
        colNum.setMaxWidth(40);
        colNum.setResizable(false);
        colNum.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().get(ComplexTable.numC)));
        colNum.setCellFactory(column -> {
            return new TableCell<Map<ComplexField, ComplexField>, ComplexField>() {
                @Override
                protected void updateItem(ComplexField item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null && !empty) {
                        if(this.getTableRow() != null) {
                            setText(this.getTableRow().getIndex()+"");
                        }
                    } else {
                        setText(null);
                        setStyle(null);
                        setGraphic(null);
                    }
                }
            };
        });
        colNum.setSortable(false);
        tableView.getColumns().add(colNum);

        colAction = new TableColumn("");
        colAction.setSortable(false);
        Button colAdd = new Button();
        colAdd.setGraphic(new ImageView(UITools.getImage("resources/images/add_col-16.png")));
        colAdd.setOnAction(actionEvent -> {
            complexTable.addColumn(-1);
            UITools.bindComplexTable(complexTable, replicate, tableView);
        });
        colAdd.disableProperty().bind(tableView.editableProperty().not());
        colAction.setGraphic(colAdd);
        colAction.setMinWidth(75);
        colAction.setMaxWidth(75);
        colAction.setPrefWidth(75);
        colAction.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().get(ComplexTable.actionC)));
        colAction.setCellFactory(mapComplexFieldTableColumn -> {
            return new TableCell<Map<ComplexField, ComplexField>, ComplexField>() {

                @Override
                protected void updateItem(ComplexField complexField, boolean empty) {
                    super.updateItem(complexField, empty);
                    setText(null);
                    setStyle(null);
                    if (complexField != null && !empty) {
                        Button add = new Button("");
                        add.setGraphic(new ImageView(UITools.getImage("resources/images/add_16.png")));
                        add.setOnAction(event -> {
                            complexTable.addLine(getIndex() + 1);
                        });
                        add.disableProperty().bind(tableView.editableProperty().not());

                        Button del = new Button("");
                        del.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));

                        del.setOnAction(event -> {
//                            if(complexTable.getContent().size() > 1) {
                                complexTable.getContent().remove(tableView.getItems().get(getIndex()));
                                complexTable.getData().setModified(true);
//                            } else {
//                                Alert a = new Alert(Alert.AlertType.ERROR);
//                                a.setContentText("1 row is mendatory. ");
//                                a.initModality(Modality.WINDOW_MODAL);
//                                a.initOwner(MainApp.primaryStage);
//                                a.showAndWait();
//                            }
                        });

                        del.disableProperty().bind(tableView.editableProperty().not());
                        HBox hb = new HBox(add, del);
                        setGraphic(hb);
                    } else {
                        setGraphic(null);
                    }
                }
            };
        });

        for(ComplexField c : complexTable.getTableHeader()) {
                if(!c.equals(ComplexTable.numC) && !c.equals(ComplexTable.actionC)) {
                    TableColumn<Map<ComplexField, ComplexField>, ComplexField> colType = new TableColumn();
                    tableView.getColumns().add(colType);
                    Button delete = new Button("");
                    delete.disableProperty().bind(tableView.editableProperty().not());
                    delete.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                    delete.setOnAction(event -> {
                        complexTable.deleteColumn(c);
                        complexTable.getData().setModified(true);
                        UITools.bindComplexTable(complexTable, replicate, tableView);
                    });
                    Button edit = new Button("");
                    edit.disableProperty().bind(tableView.editableProperty().not());
                    edit.setGraphic(new ImageView(UITools.getImage("resources/images/pencil-16.png")));
                    edit.setOnAction(event -> {
                        Dialog<ButtonType> editObject = new Dialog();
                        editObject.setResizable(true);
                        editObject.setTitle("Edit Attribute");
                        editObject.initModality(Modality.APPLICATION_MODAL);
                        editObject.initOwner(colType.getTableView().getScene().getWindow());
                        editObject.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                        editObject.getDialogPane().setPrefSize(500, 300);

                        BorderPane pane = new BorderPane();
                        Label labAttr = new Label("Attribute :");
                        TextField cAttr = new TextField();
//                        ComboBox<String> cAttr = new ComboBox<>();
                        cAttr.setPrefWidth(Double.MAX_VALUE);
                        cAttr.setEditable(true);
//                        cAttr.setValue(c.getValue().getValue());
                        cAttr.setText(c.getValue().getValue());
                        UITools.addAutoComplete(cAttr, Ontology.listAttributeProperty());
//                        UITools.addPopOver(cAttr, Ontology.Attribut);
//                        TextFields.bindAutoCompletion(cAttr, t-> {
//                            return data.listAttributeProperty();
//                        });
//                        Tools.addAutoComplete(cAttr, data.listAttributeProperty(), false);
                        Label labUnit = new Label("Unit :");
                        Button cUnit = new Button();
                        cUnit.setPrefWidth(Double.MAX_VALUE);
                        String prettyValue = Tools.getPrettyUnit(c.getUnit().getValue());
                        cUnit.setTooltip(new Tooltip(c.getUnit().getValue()));
                        if(prettyValue != null) {
                            cUnit.setText(prettyValue);
                            cUnit.setStyle(" -fx-text-fill: black;  -fx-opacity: 1;");
                        } else {
                            cUnit.setText(c.getUnit().getValue());
                            cUnit.setStyle("-fx-table-cell-border-color: red; -fx-text-fill: black; -fx-border-color: red; -fx-opacity: 1;");
                        }
                        cUnit.setOnMouseClicked(eventM -> {
                            UITools.showEditUnit(cUnit);
                        });
                        GridPane gPane = new GridPane();
                        gPane.setHgap(5);
                        gPane.add(labAttr, 0, 0);
                        gPane.add(cAttr, 0, 1);
                        GridPane.setFillWidth(cAttr, true);
                        GridPane.setFillHeight(cAttr, true);
                        gPane.add(labUnit, 1, 0);
                        gPane.add(cUnit, 1, 1);
                        GridPane.setFillWidth(cUnit, true);
                        GridPane.setFillHeight(cUnit, true);
                        ColumnConstraints c1 = new ColumnConstraints();
                        c1.setPercentWidth(65);
                        c1.setFillWidth(true);
                        ColumnConstraints c2 = new ColumnConstraints();
                        c2.setPercentWidth(35);
                        c2.setFillWidth(true);
                        gPane.getColumnConstraints().addAll(c1, c2);
                        pane.setTop(gPane);

                        GridPane gPane2 = new GridPane();
                        gPane2.setVgap(5);
                        Label labComment = new Label("Comment :");
                        TextField comment = new TextField(c.getTooltip().get());
                        comment.setPrefWidth(Double.MAX_VALUE);
                        comment.setPrefHeight(Double.MAX_VALUE);
                        gPane2.add(labComment, 0, 0);
                        gPane2.add(comment, 0, 1);
                        GridPane.setFillWidth(comment, true);
                        GridPane.setFillHeight(comment, true);

                        VBox box = new VBox();
                        ListView<TextField> listObjectView = new ListView<>();
                        box.getChildren().add(listObjectView);
                        for (String lo : c.getListObject()) {
                            if (!lo.isEmpty()) {
                                TextField comb = new TextField();
                                comb.setText(lo);
                                comb.setEditable(true);
                                UITools.addAutoComplete(comb, Ontology.listObjectProperty());
//                                UITools.addPopOver(comb, Ontology.Attribut, Ontology.Component, Ontology.Process, Ontology.Step, Ontology.Material, Ontology.Method);
                                listObjectView.getItems().add(comb);
                            }
                        }

                        listObjectView.setCellFactory(cellData -> {
                            return new ListCell<TextField>() {

                                @Override
                                protected void updateItem(TextField item, boolean empty) {
                                    super.updateItem(item, empty);
                                    if (item == null || empty) {
                                        setText(null);
                                        setStyle(null);
                                        setGraphic(null);
                                    } else {
                                        Button delButton = new Button(null, new ImageView(UITools.getImage("resources/images/del_16.png")));
                                        delButton.setOnMouseClicked(event1 -> listObjectView.getItems().remove(item));
                                        HBox hBox = new HBox();
                                        hBox.getChildren().addAll(delButton, item);
                                        hBox.maxWidthProperty().bind(listObjectView.widthProperty());
                                        setGraphic(hBox);
                                    }
                                }
                            };
                        });
                        Button addItem = new Button(null, new ImageView(UITools.getImage("resources/images/add_16.png")));
                        addItem.setOnMouseClicked(event1 -> {
                            TextField comb = new TextField();
                            comb.setEditable(true);
                            UITools.addAutoComplete(comb, Ontology.listObjectProperty());
//                            UITools.addPopOver(comb, Ontology.Attribut, Ontology.Component, Ontology.Process, Ontology.Step, Ontology.Material, Ontology.Method);
                            listObjectView.getItems().add(comb);
                        });

                        box.getChildren().add(addItem);


                        Label labObject = new Label("Objects :");
                        gPane2.add(labObject, 0,2);
                        GridPane.setFillWidth(box, true);
                        GridPane.setFillHeight(box, true);
                        box.setPrefWidth(Double.MAX_VALUE);
                        box.setPrefHeight(Double.MAX_VALUE);
                        gPane2.add(box, 0,3);
                        ColumnConstraints c20 = new ColumnConstraints();
                        c20.setPercentWidth(100);
                        c20.setFillWidth(true);
                        RowConstraints r0 = new RowConstraints();
                        r0.setPercentHeight(5.0);
                        RowConstraints r1 = new RowConstraints();
                        r1.setPercentHeight(10.0);
                        RowConstraints r2 = new RowConstraints();
                        r2.setPercentHeight(5.0);
                        RowConstraints r3 = new RowConstraints();
                        r3.setPercentHeight(80.0);
                        gPane2.getRowConstraints().addAll(r0,r1,r2,r3);
                        gPane2.getColumnConstraints().addAll(c20);
                        pane.setCenter(gPane2);
                        editObject.getDialogPane().setContent(pane);
                        Optional<ButtonType> result2 = editObject.showAndWait();
                        if (result2.get() == ButtonType.OK){
//                                c.getValue().setValue(cAttr.getValue());
                            c.getValue().setValue(cAttr.getText());

                            c.getUnit().setValue(cUnit.getTooltip().getText());
                            c.getListObject().setAll(listObjectView.getItems().stream().map(x->x.getText()).collect(Collectors.toList()));
                            c.getTooltip().setValue(comment.getText());
                            GridPane grid = new GridPane();
                            grid.add(getNode(c), 0, 0);
//                            grid.add(c.getNode(colType, complexTable.getData()), 0,0);
                            grid.add(edit, 1, 0);
                            grid.add(delete, 2, 0);
                            GridPane.setFillWidth(edit, true);
                            GridPane.setHalignment(edit, HPos.RIGHT);

                            colType.setGraphic(grid);
                            colType.styleProperty().bind(c.styleProperty());
                            complexTable.getData().setModified(true);
                        }
                    });


                    GridPane grid = new GridPane();
                    grid.add(getNode(c), 0, 0);
//                    grid.add(c.getNode(colType, complexTable.getData()), 0,0);
                    grid.add(edit, 1, 0);
                    grid.add(delete, 2, 0);
                    GridPane.setFillWidth(edit, true);
                    GridPane.setHalignment(edit, HPos.RIGHT);
                    colType.setGraphic(grid);
                    colType.styleProperty().bind(c.styleProperty());
                    colType.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().get(c)));

                    colType.setCellFactory(column -> {
                        return new TableCell<Map<ComplexField, ComplexField>, ComplexField>() {

                            @Override
                            protected void updateItem(ComplexField item, boolean empty) {
                                super.updateItem(item, empty);
                                if (item == null || empty) {
                                    setText(null);
                                    setStyle(null);
                                    setGraphic(null);
                                } else {
                                    TextField text = new TextField();
                                    text.editableProperty().bind(tableView.editableProperty());
                                    text.styleProperty().bind(item.styleProperty());
                                    item.unbindReplicatesValues();
                                    text.setText(item.getValueWithReplicate(replicateID).get());
                                    item.getValueWithReplicate(replicateID).bind(text.textProperty());
                                    item.setValuesListener(changeListener);

                                    text.setOnMouseClicked(event -> {
                                        if(text.isEditable() && event.getClickCount() == 2) {
//                                            Integer line = getIndex();

                                            Boolean symbo = c.getUnit().isEmpty().get();
                                            String attr = c.getValue().get();
//
                                            if(symbo) {
                                                Dialog<ButtonType> editValue = new Dialog();
                                                editValue.setResizable(true);
                                                editValue.setTitle("Edit symbolic value");
                                                editValue.initModality(Modality.APPLICATION_MODAL);
                                                editValue.initOwner(tableView.getScene().getWindow());
                                                editValue.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                                                editValue.getDialogPane().setPrefSize(500, 300);
                                                Label labSymbo = new Label("Value for attribute " + attr + " : ");
                                                TextField textSymbo = new TextField();
                                                textSymbo.setText(text.getText());
                                                VBox b = new VBox(4.0);
                                                b.getChildren().addAll(labSymbo, textSymbo);
                                                editValue.getDialogPane().setContent(b);
                                                Optional<ButtonType> result = editValue.showAndWait();
                                                if(result.get() == ButtonType.OK) {
                                                    text.setText(textSymbo.getText());
                                                }
                                            } else {
                                                Dialog<ButtonType> editValue = new Dialog();
                                                editValue.setResizable(true);
                                                editValue.setTitle("Edit quantity value");
                                                editValue.initModality(Modality.APPLICATION_MODAL);
                                                editValue.initOwner(tableView.getScene().getWindow());
                                                editValue.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                                                editValue.getDialogPane().setPrefSize(500, 300);
                                                String prettyUnit  = Tools.getPrettyUnit(c.getUnit().getValue());
                                                if (prettyUnit == null) {
                                                    prettyUnit = c.getUnit().getValue();
                                                }
                                                Label labQuantity = new Label("Value for attribute " + attr + " in " + prettyUnit);
                                                TextField quantValue = new TextField();
                                                StringProperty type = new SimpleStringProperty("none");
                                                quantValue.textProperty().addListener((observable, oldValue, newValue) -> {
                                                    type.setValue(Tools.getQuantValueType(newValue));
                                                });
                                                quantValue.setText(text.getText());

                                                Label labType = new Label();
                                                labType.textProperty().bind(Bindings.concat("type : ", type));
                                                VBox b = new VBox(4.0);
                                                b.getChildren().addAll(labQuantity, quantValue, labType);
                                                editValue.getDialogPane().setContent(b);
                                                Optional<ButtonType> result = editValue.showAndWait();
                                                if(result.get() == ButtonType.OK) {
                                                    text.setText(quantValue.getText());
                                                }
                                            }
//
                                        }
                                    });
                                    setGraphic(text);
                                }
                            }
                        };
                    });


                }

            }
        tableView.getColumns().add(colAction);
        tableView.setItems(complexTable.getContent());

        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    public static void bindSimpleTable(SimpleTable simpleTable, Replicate replicate, TableView tableView) {
        ChangeListener changeListener = (observable, oldValue, newValue) -> {
            simpleTable.getData().setModified(true);
        };

        tableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tableView.setTableMenuButtonVisible(true);
        Button addLine = new Button("New line ");
        addLine.disableProperty().bind(tableView.editableProperty().not());
        addLine.setGraphic(new ImageView(UITools.getImage("resources/images/add_row-16.png")));
        addLine.setOnAction(actionEvent -> {
            simpleTable.addLine(-1);
        });
        Label noContent = new Label("No content in table");
        noContent.setGraphic(addLine);
        noContent.setContentDisplay(ContentDisplay.BOTTOM);
        tableView.setPlaceholder(noContent);

        TableColumn<Map<ComplexField, ComplexField>, ComplexField> colNum = new TableColumn("#");
        colNum.setMinWidth(40);
        colNum.setMaxWidth(40);
        colNum.setResizable(false);
        colNum.setSortable(false);
        colNum.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().get(SimpleTable.numK)));
        colNum.setCellFactory(column -> {
            return new TableCell<Map<ComplexField, ComplexField>, ComplexField>() {
                @Override
                protected void updateItem(ComplexField item, boolean empty) {
                    super.updateItem(item, empty);
                    if (item != null && !empty) {
                        if(((Map<KeyWords, ComplexField>)tableView.getItems().get(getIndex())).get(SimpleTable.caractK).isEditable()) {
                            if(this.getTableRow() != null) {
                                setText(this.getTableRow().getIndex()+"");
                            }
                        } else {
                            if(this.getTableRow() != null) {
                                setText(this.getTableRow().getIndex() + "");
                            } else {
                                setText(null);
                            }
                            setStyle(null);
                            setGraphic(null);
                        }
                    } else {
                        setText(null);
                        setStyle(null);
                        setGraphic(null);
                    }
                }
            };
        });



        tableView.getColumns().add(colNum);

        TableColumn<Map<ComplexField, ComplexField>, ComplexField> colAction = new TableColumn("");
        colAction.setMinWidth(75);
        colAction.setMaxWidth(75);
        colAction.setPrefWidth(75);
        colAction.setSortable(false);
        colAction.setResizable(false);
        colAction.setCellValueFactory(cellData -> new SimpleObjectProperty<>(cellData.getValue().get(SimpleTable.actionK)));
        colAction.setCellFactory(mapComplexFieldTableColumn -> {
            return new TableCell<Map<ComplexField, ComplexField>, ComplexField>() {

                @Override
                protected void updateItem(ComplexField complexField, boolean empty) {
                    super.updateItem(complexField, empty);
                    if (complexField != null && !empty) {
                        setText(null);
                        setStyle(null);
                        Button add = new Button("");
                        add.setGraphic(new ImageView(UITools.getImage("resources/images/add_16.png")));
                        add.setOnAction(event -> {
                            simpleTable.addLine(getIndex() + 1);
                        });
                        add.disableProperty().bind(tableView.editableProperty().not());

                        Button del = new Button("");
                        del.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
                        del.setOnAction(event -> {
                            simpleTable.removeLine((Map<KeyWords, ComplexField>) tableView.getItems().get(getIndex()));
                        });
                        del.setDisable(true);
                        HBox hb = new HBox(add, del);
                        setGraphic(hb);
                        if (((Map<KeyWords, ComplexField>) tableView.getItems().get(getIndex())).get(SimpleTable.caractK).isEditable()) {
                            del.disableProperty().bind(tableView.editableProperty().not());
                        }
                    } else {
                        setText(null);
                        setStyle(null);
                        setGraphic(null);
                    }
                }
            };
        });


        for(KeyWords c : SimpleTable.getListKeyWord()) {
            TableColumn<Map<ComplexField, ComplexField>, ComplexField> colType = new TableColumn(c.getPrefLabel("en"));
            if(simpleTable.getIsACompo()) {
                if(c.equals(SimpleTable.ObjK)) {
                    colType.setText("component");
                }
            }

            colType.setCellValueFactory(cellData ->  new SimpleObjectProperty<>(cellData.getValue().get(c)));
            if(c.equals(SimpleTable.caractK)) {
                colType.setCellFactory(column -> {
                    return new TableCell<Map<ComplexField, ComplexField>, ComplexField>() {


                        @Override
                        public void updateItem(ComplexField item, boolean empty) {
                            super.updateItem(item, empty);
                            TextField combo = new TextField();
//                            ComboBox<String> combo = new ComboBox<>();

                            if (item != null && !empty) {
                                combo.maxWidthProperty().bind(widthProperty());
                                item.getValue().unbind();
                                if(item.isEditable()) {
                                    combo.editableProperty().bind(tableView.editableProperty());
                                } else {
                                    combo.editableProperty().setValue(false);

                                }
                                combo.disableProperty().bind(combo.editableProperty().not());
                                combo.setText(item.getValue().get());
                                UITools.addAutoComplete(combo, item.getListConstraint());
//                                UITools.addPopOver(combo, Ontology.Attribut);
                                item.getValue().bind(combo.textProperty());
                                item.setValuesListener(changeListener);
                                combo.styleProperty().bind(item.styleProperty());
                                setGraphic(combo);
                            } else {
                                setText(null);
                                setStyle(null);
                                setGraphic(null);
                            }
                        }
                    };
                });
            } else {

                if (c.equals(SimpleTable.unitK)) {
                    colType.setCellFactory(column -> {
                        return new TableCell<Map<ComplexField, ComplexField>, ComplexField>() {


                            @Override
                            public void updateItem(ComplexField item, boolean empty) {
                                super.updateItem(item, empty);
//                                TextField textUnit = new TextField();
                                Button textUnit = new Button();

                                if (item != null && !empty) {
                                    textUnit.maxWidthProperty().bind(widthProperty());
                                    item.getValue().unbind();
//                                    if(item.isEditable()) {
//                                        textUnit.editableProperty().bind(tableView.editableProperty());
//                                    } else {
//                                        textUnit.editableProperty().setValue(false);
//                                    }
                                    textUnit.disableProperty().bind(tableView.editableProperty().not());
                                    textUnit.setTooltip(new Tooltip(item.getValue().get()));
                                    String prettyValue = Tools.getPrettyUnit(item.getValue().get());
                                    if(prettyValue != null) {
                                        textUnit.setText(prettyValue);
                                        textUnit.setStyle(" -fx-text-fill: black;  -fx-opacity: 1;");
                                    } else {
                                        textUnit.setText(item.getValue().get());
                                        textUnit.setStyle("-fx-table-cell-border-color: red; -fx-text-fill: black; -fx-border-color: red; -fx-opacity: 1;");
                                    }
                                    item.getValue().bind(textUnit.textProperty());
                                    item.getValue().bind(textUnit.tooltipProperty().getValue().textProperty());


                                    textUnit.setOnMouseClicked(event -> {
                                        if (!textUnit.isDisable()) {
                                            UITools.showEditUnit(textUnit);
                                        }
                                    });
                                    setGraphic(textUnit);
                                } else {
                                    setText(null);
                                    setStyle(null);
                                    setGraphic(null);
                                }
                            }
                        };
                    });
                } else {
                    if (c.equals(SimpleTable.ObjK)) {
                        colType.setCellFactory(column -> {
                            return new TableCell<Map<ComplexField, ComplexField>, ComplexField>() {


                                @Override
                                public void updateItem(ComplexField item, boolean empty) {
                                    super.updateItem(item, empty);


                                    if (item != null && !empty) {
                                        if(!simpleTable.getIsACompo()) {
                                            Button b = new Button();
                                            b.maxWidthProperty().bind(widthProperty());
                                            b.textProperty().bind(item.getValue());
                                            b.styleProperty().bind(item.styleProperty());
                                            b.disableProperty().bind(tableView.editableProperty().not());
                                            b.setOnMouseClicked(event -> {
                                                Dialog<String> editObject = new Dialog();
                                                editObject.setResizable(true);
                                                editObject.setTitle("Edit objects");
                                                editObject.initModality(Modality.APPLICATION_MODAL);
                                                editObject.initOwner(tableView.getScene().getWindow());
                                                editObject.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                                                editObject.getDialogPane().setPrefSize(500, 300);
                                                VBox box = new VBox();
                                                ListView<TextField> listObject = new ListView<>();
                                                box.getChildren().add(listObject);
                                                String[] tableString = item.getValue().get().split("::");
                                                for (String c : tableString) {
                                                    if (!c.isEmpty()) {
                                                        TextField comb = new TextField();
                                                        comb.setText(c);
                                                        comb.setEditable(true);
                                                        UITools.addAutoComplete(comb, item.getListConstraint());
//                                                        UITools.addPopOver(comb, Ontology.Attribut, Ontology.Component, Ontology.Step, Ontology.Material, Ontology.Method);
                                                        listObject.getItems().add(comb);
                                                    }
                                                }
                                                listObject.setCellFactory(cellData -> {
                                                    return new ListCell<TextField>() {

                                                        @Override
                                                        protected void updateItem(TextField item, boolean empty) {
                                                            super.updateItem(item, empty);
                                                            if (item == null || empty) {
                                                                setText(null);
                                                                setStyle(null);
                                                                setGraphic(null);
                                                            } else {
                                                                Button delButton = new Button(null, new ImageView(UITools.getImage("resources/images/del_16.png")));
                                                                delButton.setOnMouseClicked(event1 -> listObject.getItems().remove(item));
                                                                HBox hBox = new HBox();
                                                                hBox.getChildren().addAll(delButton, item);
                                                                hBox.maxWidthProperty().bind(listObject.widthProperty());
                                                                setGraphic(hBox);
                                                            }
                                                        }
                                                    };
                                                });
                                                Button addItem = new Button(null, new ImageView(UITools.getImage("resources/images/add_16.png")));
                                                addItem.setOnMouseClicked(event1 -> {
                                                    TextField comb = new TextField();
                                                    comb.setEditable(true);
                                                    UITools.addAutoComplete(comb, item.getListConstraint());
//                                                    UITools.addPopOver(comb, Ontology.Attribut, Ontology.Component, Ontology.Step, Ontology.Material, Ontology.Method);
                                                    listObject.getItems().add(comb);
                                                });

                                                box.getChildren().add(addItem);
                                                editObject.getDialogPane().setContent(box);

                                                editObject.setResultConverter(dialogButton -> {
                                                    if (dialogButton == ButtonType.OK) {
                                                        return listObject.getItems().stream().map(x -> x.getText()).collect(Collectors.joining("::"));
                                                    }
                                                    return null;
                                                });

                                                Optional<String> result = editObject.showAndWait();
                                                result.ifPresent(newObjects -> {
                                                    item.setValue(newObjects);
                                                    simpleTable.getData().setModified(true);
                                                });

                                            });

                                            setGraphic(b);
                                        } else {


                                            if (item != null && !empty) {
                                                TextField combo = new TextField();
                                                combo.maxWidthProperty().bind(widthProperty());
                                                item.getValue().unbind();
                                                if(item.isEditable()) {
                                                    combo.editableProperty().bind(tableView.editableProperty());
                                                } else {
                                                    combo.editableProperty().setValue(false);
                                                }
                                                combo.disableProperty().bind(combo.editableProperty().not());
                                                combo.setText(item.getValue().get());
                                                UITools.addAutoComplete(combo, item.getListConstraint());
//                                                UITools.addPopOver(combo, Ontology.Component);
                                                item.getValue().bind(combo.textProperty());
                                                item.setValuesListener(changeListener);
                                                combo.styleProperty().bind(item.styleProperty());
                                                setGraphic(combo);
                                            } else {
                                                setText(null);
                                                setStyle(null);
                                                setGraphic(null);
                                            }
                                        }
                                    } else {
                                        setText(null);
                                        setStyle(null);
                                        setGraphic(null);
                                    }
                                }
                            };
                        });
                    } else {
                        if (c.equals(SimpleTable.valK)) {
                            colType.setCellFactory(column -> {
                                return new TableCell<Map<ComplexField, ComplexField>, ComplexField>() {

                                    @Override
                                    protected void updateItem(ComplexField item, boolean empty) {
                                        super.updateItem(item, empty);
                                        if (item == null || empty) {
                                            setText(null);
                                            setStyle(null);
                                            setGraphic(null);
                                        } else {
                                            if (!item.isConstained()) { // valeur -> soit quantité soit Measurement Scale
                                                TextField text = new TextField();
                                                text.editableProperty().bind(tableView.editableProperty());
                                                text.styleProperty().bind(item.styleProperty());
                                                item.unbindReplicatesValues();
                                                int replicateID = replicate != null ? replicate.getId() : 0;
                                                text.setText(item.getValueWithReplicate(replicateID).getValue());
                                                item.getValueWithReplicate(replicateID).bind(text.textProperty());
                                                item.setValuesListener(changeListener);

                                                text.setOnMouseClicked(event -> {
                                                    if (text.isEditable() && event.getClickCount() == 2) {
                                                        Integer line = getIndex();
                                                        TableColumn<Map<ComplexField, ComplexField>, ComplexField> colUnit = (TableColumn<Map<ComplexField, ComplexField>, ComplexField>) tableView.getColumns().stream().filter(o -> ((TableColumn)o).getText().equalsIgnoreCase("unit")).findFirst().orElse(null);
                                                        TableColumn<Map<ComplexField, ComplexField>, ComplexField> colcaract = (TableColumn<Map<ComplexField, ComplexField>, ComplexField>) tableView.getColumns().stream().filter(o -> ((TableColumn)o).getText().equalsIgnoreCase("attribute")).findFirst().orElse(null);
//                                                        TableColumn<Map<ComplexField, ComplexField>, ComplexField> colUnit = simpleTable.getTableColumnByName(tableView, "unit");
//                                                        TableColumn<Map<ComplexField, ComplexField>, ComplexField> colcaract = simpleTable.getTableColumnByName(tableView, "caracteristic");

                                                        Boolean symbo = colUnit.getCellData(line).getValue().isEmpty().get();
                                                        String attr = colcaract.getCellData(line).getValue().get();

                                                        if (symbo) {
                                                            Dialog<ButtonType> editValue = new Dialog();
                                                            editValue.setResizable(true);
                                                            editValue.setTitle("Edit symbolic value");
                                                            editValue.initModality(Modality.APPLICATION_MODAL);
                                                            editValue.initOwner(tableView.getScene().getWindow());
                                                            editValue.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                                                            editValue.getDialogPane().setPrefSize(500, 300);
                                                            Label labSymbo = new Label("Value for attribute " + attr + " : ");
                                                            TextField textSymbo = new TextField();
                                                            textSymbo.setText(text.getText());
                                                            VBox b = new VBox(4.0);
                                                            b.getChildren().addAll(labSymbo, textSymbo);
                                                            editValue.getDialogPane().setContent(b);
                                                            Optional<ButtonType> result = editValue.showAndWait();
                                                            if (result.get() == ButtonType.OK) {
                                                                text.setText(textSymbo.getText());
                                                            }
                                                        } else {
                                                            Dialog<ButtonType> editValue = new Dialog();
                                                            editValue.setResizable(true);
                                                            editValue.setTitle("Edit quantity value");
                                                            editValue.initModality(Modality.APPLICATION_MODAL);
                                                            editValue.initOwner(tableView.getScene().getWindow());
                                                            editValue.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL, ButtonType.OK);
                                                            editValue.getDialogPane().setPrefSize(500, 300);
                                                            Label labQuantity = new Label("Value for attribute " + attr + " in " + colUnit.getCellData(line).getValue().get());
                                                            TextField quantValue = new TextField();
                                                            StringProperty type = new SimpleStringProperty("none");
                                                            quantValue.textProperty().addListener((observable, oldValue, newValue) -> {
                                                                type.setValue(Tools.getQuantValueType(newValue));
                                                            });
                                                            quantValue.setText(text.getText());

                                                            Label labType = new Label();
                                                            labType.textProperty().bind(Bindings.concat("type : ", type));
                                                            VBox b = new VBox(4.0);
                                                            b.getChildren().addAll(labQuantity, quantValue, labType);
                                                            editValue.getDialogPane().setContent(b);
                                                            Optional<ButtonType> result = editValue.showAndWait();
                                                            if (result.get() == ButtonType.OK) {
                                                                text.setText(quantValue.getText());
                                                            }
                                                        }

                                                    }
                                                });

                                                setGraphic(text);
                                            } else {
                                                TextField combo = new TextField();
                                                combo.maxWidthProperty().bind(widthProperty());
                                                item.unbindReplicatesValues();
                                                if (item.isEditable()) {
                                                    combo.editableProperty().bind(tableView.editableProperty());
                                                } else {
                                                    combo.editableProperty().setValue(false);
                                                }
                                                combo.disableProperty().bind(combo.editableProperty().not());
                                                combo.setText(item.getValue().get());
                                                UITools.addAutoComplete(combo, item.getListConstraint());
                                                int replicateID = replicate != null ? replicate.getId() : 0;
                                                item.getValueWithReplicate(replicateID).bind(combo.textProperty());

                                                combo.styleProperty().bind(item.styleProperty());
                                                setGraphic(combo);
                                                item.setValuesListener(changeListener);
                                            }
                                        }
                                    }
                                };
                            });
                        } else {
                            colType.setCellFactory(column -> {
                                return new TableCell<Map<ComplexField, ComplexField>, ComplexField>() {

                                    @Override
                                    protected void updateItem(ComplexField item, boolean empty) {
                                        super.updateItem(item, empty);
                                        if (item == null || empty) {
                                            setText(null);
                                            setStyle(null);
                                            setGraphic(null);
                                        } else {
                                            TextField text = new TextField();
                                            text.editableProperty().bind(tableView.editableProperty());
                                            text.styleProperty().bind(item.styleProperty());
                                            item.getValue().unbind();
                                            text.setText(item.getValue().get());
                                            item.getValue().bind(text.textProperty());
                                            item.setValuesListener(changeListener);
                                            setGraphic(text);
                                        }
                                    }
                                };
                            });
                        }
                    }
                }
            }

            tableView.getColumns().add(colType);
        }
        tableView.getColumns().add(colAction);
        tableView.setColumnResizePolicy(tableView.CONSTRAINED_RESIZE_POLICY);
        tableView.setItems(simpleTable.getContent());
    }

    public static void createNewSkosScheme() {
        if(MainApp.getOntologyControler() != null && MainApp.getOntologyControler().getCurrentOntology() != null) {
            Ontology onto = MainApp.getOntologyControler().getCurrentOntology();
            TextInputDialog dialogNewScheme = new TextInputDialog("");
            dialogNewScheme.setTitle("Skos Scheme");
            dialogNewScheme.setHeaderText("Create new Skos Scheme");
            dialogNewScheme.initOwner(MainApp.primaryStage);
            Optional<String> result = dialogNewScheme.showAndWait();
            if (result.isPresent()) {
                if (!result.get().isEmpty()) {
                    String sch = result.get();
                    if (onto.getlistConceptScheme().stream().filter(skosScheme -> skosScheme.getName().equalsIgnoreCase(sch)).count() == 0) {
                        MainApp.getOntologyControler().createSkosScheme(sch);
                        MainApp.updateConceptScheme();
                    } else {
                        Alert schemeExist = new Alert(Alert.AlertType.ERROR);
                        schemeExist.setTitle("Error creating skos concept scheme");
                        schemeExist.setHeaderText("An er occured during the creation");
                        schemeExist.setContentText("A Skos concept scheme with this name already exist");
                        schemeExist.initOwner(MainApp.primaryStage);
                        schemeExist.initModality(Modality.APPLICATION_MODAL);
                        schemeExist.showAndWait();
                    }
                }
            }
        }
    }

}
