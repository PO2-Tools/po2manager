/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.utils;

import org.update4j.Configuration;
import org.update4j.FileMetadata;
import org.update4j.OS;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Update4JConf {

    public static final Pattern MY_OS_PATTERN = Pattern.compile(".+j2v8_(linux|win|mac).+");


    public static void main(String[] args) {
        System.out.println("Creating conf");
        var cb = Configuration.builder()

                // base URI from where to download, overridable in
                // each individual file setting
                .baseUri("https://quantum.mia-ps.inrae.fr/PO2/download/")
                .launcher("fr.inra.po2vocabmanager.MainApp")

                // base path where to save on client machine, overridable in
                // each individual file setting
                .basePath("${app.dir}/app/business/")

                // List this property
                .property("app.name", "PO2Manager")
                .property("app.dir", "${user.home}/${app.name}")
                .property("app.dir" ,"${LOCALAPPDATA}/${app.name}" , OS.WINDOWS)

                .property("app.bootstrap.location", "${app.dir}/app/bootstrap/")


                // Automatically resolves system property
//                .property("user.location", "${user.home}/po2manager/")

                // List this file, uri and path are same as filename
                // Read metadata from real file on dev machine
                // Will be dynamically loaded on the modulepath
                // PO2Manager-1.5.0.4-SNAPSHOT.jar} will be replaced by ant task in maven build
                .files(FileMetadata.streamDirectory("target")
//                        .filter(r -> r.getSource().toString().endsWith("jar"))
                        .filter(r -> r.getSource().getFileName().toString().matches("^PO2Manager(.*).jar$"))
                        .peek(f->f.path(f.getSource().getFileName()).classpath()))
                .file(FileMetadata.readFrom("src/main/resources/resources/images/Splash.png")
                        .uri("resources/Splash.png")
                        .path("${app.dir}/resources_updater/Splash.png"))
                .files(FileMetadata.streamDirectory("src/main/resources/resources/PO2Updater")
                        .peek(f->f.uri("resources/"+f.getSource().getFileName()))
                        .peek(f->f.path("${app.dir}/resources_updater/"+f.getSource().getFileName())))

        // Path will be same as uri
        // Will be dynamically loaded on the classpath
//                .file(FileMetadata.readFrom("myFile2.jar")
//                        .uri("file.zip")
//                        .classpath())

        // Will not be loaded on any path, ideal for bootstrap dependencies
        // that are loaded on JVM startup,
        // or non jar files
//                .file(FileMetadata.readFrom("my-resource.png")
//                        .path("${user.location}/my-resource.png")
//                        .uri("https://example.com/pix/my-picture.png"))

        // adds the whole directory and marks all jar files with 'classpath'
        // getSource() returns the path of the real file.
//            .files(FileMetadata.streamDirectory("resources")
//                .peek(f -> f.uri(f.getSource().toString()))
//                    .peek(f-> f.path(f.getSource())))

             .files(FileMetadata.streamDirectory("target/dependency")
                .peek(r -> r.classpath(r.getSource().toString().endsWith(".jar")))
                .peek(f -> f.uri("dependencies/"+f.getSource().getFileName()))
                     .peek(f->f.path("dependencies/"+f.getSource().getFileName()))
                     .peek(Update4JConf::getOsFromFileName)
                .peek(f->f.ignoreBootConflict(false)));


// Once all settings are set, let's build it
        Configuration config = cb.build();
        try(Writer out = new FileWriter("setup.xml")) {
            config.write(out);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static FileMetadata.Reference getOsFromFileName(FileMetadata.Reference reference) {
        FileMetadata.Reference ref = reference.osFromFilename();
        if(ref.getOs() == null) {
            Matcher osMatcher = MY_OS_PATTERN.matcher(ref.getSource().toString());
            if (osMatcher.matches()) {
                ref.os(OS.fromShortName(osMatcher.group(1))) ;
            }
        }

        if(ref.getOs() == null) {

        } else {
            System.out.println("os found : " + ref.getOs() + " for " + ref.getSource().getFileName().toString());
        }
        return ref;
    }

}
