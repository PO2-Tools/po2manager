/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.utils;

import fr.inra.po2vocabmanager.MainApp;
import fr.inrae.po2engine.model.SkosScheme;
import fr.inrae.po2engine.model.VocabConcept;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

public class XCellScheme extends TableCell<SkosScheme, SkosScheme> {
    HBox hbox = new HBox();
    Label label = new Label("");
    Pane pane = new Pane();
    Button button = new Button("");
    VocabConcept node = null;

    public XCellScheme(VocabConcept node) {
        super();
        this.node = node;
        button.setGraphic(new ImageView(UITools.getImage("resources/images/del_16.png")));
        hbox.getChildren().addAll(label, pane, button);
        HBox.setHgrow(pane, Priority.ALWAYS);
        button.setOnAction(event -> getTableView().getItems().remove(getItem()));
        button.disableProperty().bind(MainApp.getOntologyControler().getCanEditProperty().not());
    }


    @Override
    public void updateItem(SkosScheme item, boolean empty) {
        super.updateItem(item, empty);
        setText(null);
        if(item == null || empty) {
            setGraphic(null);
        } else {
            label.setText(item.getName());
            if(!node.isDeletable()) {
                button.setVisible(false);
            }
            if(item.getName().equalsIgnoreCase("main")) {
                button.setVisible(false);
            }
            setGraphic(hbox);
        }
    }
}
