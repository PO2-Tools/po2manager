/*
 * Copyright (c) 2023. INRAE
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the “Software”), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inra.po2vocabmanager.view;

import fr.inra.po2vocabmanager.MainApp;
import fr.inra.po2vocabmanager.utils.UITools;
import fr.inrae.po2engine.externalTools.CloudConnector;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.tools.ant.taskdefs.Manifest;
import org.apache.tools.ant.taskdefs.ManifestException;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ResourceBundle;

/**
 * Created by stephane on 03/07/17.
 */
public class SplashController implements Initializable {

    @FXML
    private StackPane rootPane;
    @FXML
    private Text splashText;
    @FXML
    private ImageView background;
    @FXML
    private ProgressBar progressBar;

    private MainApp mainApp;
    private DoubleProperty doubleProgress;
    private HttpClientBuilder httpBuilder;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        background.setImage(UITools.getImage("resources/images/Splash.png"));
        doubleProgress = new SimpleDoubleProperty(0.0);
        progressBar.progressProperty().bind(doubleProgress);


        try {
            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());

            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    builder.build(), NoopHostnameVerifier.INSTANCE);
            httpBuilder = HttpClientBuilder.create().setSSLSocketFactory(sslsf);


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }


    }

    public void startSplash(MainApp mainApp) {
        this.mainApp = mainApp;
        new SplashScreen().start();
    }

    class SplashScreen extends Thread {
        String updateVersion = "";
        String downloadURL = "";
        String changelog = null;

        @Override
        public void run() {
            try {
                Manifest m = new Manifest(new InputStreamReader(MainApp.class.getClassLoader().getResourceAsStream("resources/manifest")));
                Manifest.Section section = m.getSection("app_info");
                Manifest.Attribute attrVersion = section.getAttribute("Specification-Version");
                Manifest.Attribute attrCompDate = section.getAttribute("Compilation-Date");

                if (attrVersion != null && !"".equals(attrVersion.getValue())) {
                    String versionDate = "Version : " + attrVersion.getValue() + " - " + attrCompDate.getValue();
                    MainApp.setVersionDate(versionDate);
                    splashText.setText(versionDate);
                    MainApp.logger.debug("loading PO²Manager : " + versionDate);
                }


                CloudConnector.reset();
                CloudConnector.listOntology(doubleProgress, 0.5);
                CloudConnector.listData(doubleProgress,0.5);
                CloudConnector.refreshPlatform();
                Thread.sleep(2000);
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        mainApp.initRootLayout();
                        mainApp.showChoiceOverview();
                        rootPane.getScene().getWindow().hide();
                    }
                });

            } catch (InterruptedException ex) {
                MainApp.logger.error("splash can't start", ex);
            } catch (IOException | ManifestException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
