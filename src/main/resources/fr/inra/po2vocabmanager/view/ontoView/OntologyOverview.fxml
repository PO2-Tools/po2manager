<?xml version="1.0" encoding="UTF-8"?>

<!--
  ~ Copyright (c) 2023. INRAE
  ~ Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
  ~ documentation files (the “Software”), to deal in the Software without restriction, including without limitation
  ~ the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
  ~ and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
  ~
  ~ The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  ~
  ~ THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
  ~ BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
  ~ NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
  ~ DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  ~ OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
  ~
  ~ SPDX-License-Identifier: MIT
  -->

<?import javafx.geometry.Insets?>
<?import javafx.scene.control.*?>
<?import javafx.scene.image.ImageView?>
<?import javafx.scene.layout.*?>
<?import javafx.scene.text.*?>
<AnchorPane xmlns:fx="http://javafx.com/fxml/1" maxHeight="1.7976931348623157E308" maxWidth="1.7976931348623157E308" prefHeight="706.0" prefWidth="1080.0" xmlns="http://javafx.com/javafx/null" fx:controller="fr.inra.po2vocabmanager.view.ontoView.OntologyOverviewController">
    <children>
        <SplitPane dividerPositions="0.25" prefHeight="300.0" prefWidth="600.0" AnchorPane.bottomAnchor="0.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0">
            <items>
                <AnchorPane minHeight="0.0" minWidth="0.0" prefHeight="160.0" prefWidth="100.0">
                    <children>
                        <BorderPane prefHeight="598.0" prefWidth="234.0" AnchorPane.bottomAnchor="0.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0">
                            <center>
                                <TreeView fx:id="vocabTree" editable="true" prefHeight="298.0" prefWidth="174.0" showRoot="false" BorderPane.alignment="CENTER" />
                            </center>
                            <bottom>
                                <HBox fx:id="statusBox" maxHeight="100.0" minHeight="100.0" prefHeight="100.0" prefWidth="200.0" BorderPane.alignment="CENTER" />
                            </bottom>
                        </BorderPane>
                    </children>
                </AnchorPane>
                <AnchorPane fx:id="anchorPane" minHeight="0.0" minWidth="0.0" prefHeight="160.0" prefWidth="100.0">
                    <children>
                        <BorderPane fx:id="borderPane" prefHeight="598.0" prefWidth="556.0" AnchorPane.bottomAnchor="0.0" AnchorPane.leftAnchor="0.0" AnchorPane.rightAnchor="0.0" AnchorPane.topAnchor="0.0">
                     <center>
                        <VBox maxHeight="1.7976931348623157E308" maxWidth="1.7976931348623157E308" BorderPane.alignment="CENTER">
                           <children>
                                      <TableView fx:id="tableView" editable="true" maxHeight="1.7976931348623157E308" minHeight="180.0" prefHeight="180.0" tableMenuButtonVisible="true" VBox.vgrow="ALWAYS">
                                          <columns>
                                              <TableColumn fx:id="flagColumn" maxWidth="40.0" minWidth="40.0" prefWidth="40.0" />
                                              <TableColumn fx:id="labelColumn" maxWidth="400.0" minWidth="150.0" prefWidth="200.0" sortable="false" text="Labels" />
                                    <TableColumn fx:id="synonymsColumn" maxWidth="400.0" minWidth="150.0" prefWidth="200.0" text="Synonyms" />
                                              <TableColumn fx:id="descColumn" prefWidth="-1.0" sortable="false" text="Definition" />
                                          </columns>
                                          <columnResizePolicy>
                                              <TableView fx:constant="CONSTRAINED_RESIZE_POLICY" />
                                          </columnResizePolicy>
                                      </TableView>
                              <HBox minHeight="130.0" spacing="5.0" VBox.vgrow="ALWAYS">
                                 <children>
                                    <TitledPane animated="false" collapsible="false" maxHeight="1.7976931348623157E308" maxWidth="1.7976931348623157E308" text="Scope Note" textAlignment="CENTER" HBox.hgrow="SOMETIMES">
                                       <content>
                                          <TextArea fx:id="textScopeNote" maxHeight="1.7976931348623157E308" maxWidth="1.7976931348623157E308" />
                                       </content>
                                       <font>
                                          <Font name="System Bold" size="13.0" />
                                       </font>
                                    </TitledPane>
                                    <TitledPane animated="false" collapsible="false" maxHeight="1.7976931348623157E308" maxWidth="1.7976931348623157E308" text="Editorial Note" textAlignment="CENTER" HBox.hgrow="SOMETIMES">
                                       <content>
                                          <TextArea fx:id="textEditorialNote" maxHeight="1.7976931348623157E308" maxWidth="1.7976931348623157E308" />
                                       </content>
                                       <font>
                                          <Font name="System Bold" size="13.0" />
                                       </font>
                                    </TitledPane>
                                 </children>
                              </HBox>
                              <HBox minHeight="130.0" spacing="5.0" VBox.vgrow="SOMETIMES">
                                 <children>
                                              <TableView fx:id="tableExact" HBox.hgrow="ALWAYS">
                                                  <columns>
                                                      <TableColumn fx:id="exactMatchColumn" editable="false" maxWidth="1.7976931348623157E308" minWidth="-1.0" prefWidth="-1.0" sortable="false" text="Exact Match" />
                                                  </columns>
                                       <columnResizePolicy>
                                          <TableView fx:constant="CONSTRAINED_RESIZE_POLICY" />
                                       </columnResizePolicy>
                                              </TableView>
                                    <TableView fx:id="tableClose" HBox.hgrow="ALWAYS">
                                      <columns>
                                        <TableColumn fx:id="closeMatchColumn" editable="false" maxWidth="1.7976931348623157E308" minWidth="-1.0" prefWidth="-1.0" sortable="false" text="Close Match" />
                                      </columns>
                                       <columnResizePolicy>
                                          <TableView fx:constant="CONSTRAINED_RESIZE_POLICY" />
                                       </columnResizePolicy>
                                    </TableView>
                                 </children>
                              </HBox>
                           </children>
                        </VBox>
                     </center>
                     <top>
                        <HBox maxHeight="104.0" minHeight="104.0" prefHeight="104.0" spacing="5.0" BorderPane.alignment="CENTER">
                           <children>
                                      <ImageView fx:id="imageNode" fitHeight="104.0" fitWidth="161.0" pickOnBounds="true" preserveRatio="true" HBox.hgrow="NEVER" />
                              <VBox maxHeight="104.0" minHeight="104.0" prefHeight="104.0" HBox.hgrow="ALWAYS">
                                 <children>
                                              <Button fx:id="buttonCopy" mnemonicParsing="false" visible="false">
                                                  <font>
                                                      <Font size="30.0" />
                                                  </font>
                                              </Button>
                                              <Hyperlink fx:id="link" />
                                    <Label fx:id="labelChemin" visible="false" />
                                 </children>
                                 <HBox.margin>
                                    <Insets left="10.0" right="10.0" />
                                 </HBox.margin>
                              </VBox>
                              <VBox fx:id="boxDimension" maxHeight="104.0" maxWidth="150.0" minHeight="104.0" minWidth="150.0" prefHeight="104.0" prefWidth="150.0" HBox.hgrow="NEVER">
                                 <children>
                                    <Label text="Dimensions">
                                       <font>
                                          <Font size="16.0" />
                                       </font>
                                    </Label>
                                 </children>
                              </VBox>
                           </children>
                        </HBox>
                     </top>
                     <bottom>
                        <HBox maxHeight="150.0" minHeight="150.0" prefHeight="150.0" spacing="5.0" BorderPane.alignment="CENTER">
                           <children>
                                        <TableView fx:id="tableViewConceptScheme" maxWidth="200.0" minWidth="200.0" prefHeight="100.0" prefWidth="200.0">
                                            <columns>
                                                <TableColumn fx:id="conceptSchemeColumn" prefWidth="75.0" text="Concept scheme" />
                                            </columns>
                                            <columnResizePolicy>
                                                <TableView fx:constant="CONSTRAINED_RESIZE_POLICY" />
                                            </columnResizePolicy>
                                        </TableView>
                              <TitledPane animated="false" collapsible="false" text="Note" textAlignment="CENTER" HBox.hgrow="ALWAYS">
                                 <font>
                                    <Font name="System Bold" size="13.0" />
                                 </font>
                                 <content>
                                    <TextArea fx:id="textNote" prefHeight="200.0" prefWidth="200.0" />
                                 </content>
                              </TitledPane>
                                        <ImageView fx:id="imageCloud" fitHeight="150.0" fitWidth="300.0" pickOnBounds="true" preserveRatio="true" HBox.hgrow="NEVER" />
                           </children>
                        </HBox>
                     </bottom>
                        </BorderPane>
                    </children>
                </AnchorPane>
            </items>
        </SplitPane>
    </children>
</AnchorPane>
